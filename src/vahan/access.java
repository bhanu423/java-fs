package vahan;

public class access {
	 static fileSystemInterface fileSystem = new fileSystem();

	  public static void main(String[] args) {
	    fileSystem.create("f1.txt", "file 1");
	    fileSystem.create("f2.txt",
	        "This is a large file. Text in file 2. This is a large file. Text in file 2. This is a large file. Text in file 3. This is a large file. Text in file 2. This is a large file. Text in file 2.");
	    System.out.println("reading f1.txt");
	    System.out.println(fileSystem.read("f1.txt"));
	    fileSystem.update("f1.txt", "Updated file 1 content");
	    System.out.println(fileSystem.read("f1.txt", 5, 10));
	    System.out.println("read updated f1.txt");
	    System.out.println(fileSystem.read("f2.txt"));
	    System.out.println(fileSystem.read("f2.txt"));
	    fileSystem.delete("f1.txt");
	    System.out.println(fileSystem.read("f1.txt"));
	    System.out.println(fileSystem.read("f2.txt"));
	    fileSystem.create("f3.txt","This is new text in file 3.");
	    fileSystem.delete("f2.txt");
	    System.out.println(fileSystem.read("f3.txt"));
	  }
}
