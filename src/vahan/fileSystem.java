package vahan;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


//import io.netty.buffer.ByteBuf; can be used if immediate release of memory release is required


public class fileSystem implements fileSystemInterface {
	ArrayList<ByteBuffer> store = new ArrayList<ByteBuffer>(5);

	Map<String, int[]> refs = new HashMap<String, int[]>();

	@Override
	public void create(String name, String content) {
		byte[] dataBytes = null;
		try {
			dataBytes = content.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		int slotsRequired = (int) Math.ceil(dataBytes.length / 10.0);
		int currLength = store.size();
		for (int x = 0; x < slotsRequired; x++) {
			byte[] toCopy = Arrays.copyOfRange(dataBytes, x * 10, (x + 1) * 10);
			store.add(ByteBuffer.wrap(toCopy));
		}
		int newLength = store.size();
		int[] sizeRefs = new int[3];
		sizeRefs[0] = currLength;
		sizeRefs[1] = newLength;
		sizeRefs[2] = dataBytes.length;
		this.refs.put(name, sizeRefs);
	}

	@Override
	public String read(String name) {
		int[] fileMeta = this.refs.get(name);
		if (fileMeta == null) {
			return "File does not exist";
		}
		List<ByteBuffer> fileList = this.store.subList(fileMeta[0], fileMeta[1]);
		String result = fileList.stream().map(n -> StandardCharsets.UTF_8.decode(n.rewind()).toString())
				.collect(Collectors.joining("", "", ""));
		return result;
		
	}

	@Override
	public String read(String name, int fromByte, int size) {
		int[] fileMeta = this.refs.get(name);
		if (fileMeta == null) {
			return "File does not exist";
		}
		int readStart = (int) (fileMeta[0] + Math.floor(fromByte / 10.0));
		int readEnd = (int) (fileMeta[0] + Math.ceil(size / 10.0));
		if (fileMeta[2] < fromByte + size) {
			size = 0;
		}

		if (fileMeta[1] < readEnd) {
			// Handle outbound access
			readEnd = fileMeta[1];
		}

		List<ByteBuffer> fileList = this.store.subList(readStart, readEnd+1);
		String result = fileList.stream().map(n -> StandardCharsets.UTF_8.decode(n).toString())
				.collect(Collectors.joining("", "", ""));
		return result.substring(fromByte, fromByte + size);
	}

	@Override
	public void update(String name, String content) {
		int[] fileMeta = this.refs.get(name);
		if (fileMeta == null) {
			return;
		}
		int rangetoBeDeleted = fileMeta[1] - fileMeta[0];
		byte[] dataBytes = null;
		try {
			dataBytes = content.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		int slotsRequired = (int) Math.ceil(dataBytes.length / 10.0);
		int currLength = store.size();
		for (int x = 0; x < slotsRequired; x++) {
			byte[] toCopy = Arrays.copyOfRange(dataBytes, x * 10, (x + 1) * 10);
			store.add(ByteBuffer.wrap(toCopy));
		}
		int newLength = store.size();
		int[] sizeRefs = new int[3];
		sizeRefs[0] = currLength;
		sizeRefs[1] = newLength;
		sizeRefs[2] = dataBytes.length;
		this.refs.put(name, sizeRefs);

		this.refs.forEach((k, v) -> {
			if (v[0] > fileMeta[0]) {
				v[0] = v[0] - rangetoBeDeleted;
				v[1] = v[1] - rangetoBeDeleted;
			}
		});

		this.store.subList(fileMeta[0], fileMeta[1]).clear();
	}

	@Override
	public void delete(String name) {
		int[] fileMeta = this.refs.get(name);
		int rangetoBeDeleted = fileMeta[1] - fileMeta[0];
		 this.refs.forEach((k, v) -> {
				if (v[0] > fileMeta[0]) {
					v[0] = v[0] - rangetoBeDeleted;
					v[1] = v[1] - rangetoBeDeleted;
				}
			});
		 this.store.subList(fileMeta[0], fileMeta[1]).clear();
		 this.refs.remove(name);
	}

}
