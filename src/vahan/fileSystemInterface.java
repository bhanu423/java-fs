package vahan;

public interface fileSystemInterface {
	void create(String name, String content);
	//returns the entire file content as string
	String read(String name);
	//returns size bytes of file as String starting from fromByte
	String read(String name, int fromByte, int size);
	//updates the file with given content
	void update(String name, String content);
	//deletes the file
	void delete(String name);
}
